package com.qiqv

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.ui.Messages

/**
 * @author hjr
 * @version 1.0
 * @date 2023/7/14 16:01
 */
class ShowProjectNameAction : AnAction() {
    override fun actionPerformed(e: AnActionEvent) {
        val project = e.project
        Messages.showMessageDialog("项目名称为:" + project!!.name, "这是标题", Messages.getInformationIcon())
    }
}
