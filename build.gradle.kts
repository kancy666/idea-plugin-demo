// 项目依赖的插件，默认会依赖kotlin，但我们这里是直接用java来开发插件的，所以这里依赖我们可以去掉
plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.8.21"
    id("org.jetbrains.intellij") version "1.13.3"
}

// 插件的一些基本信息，按实际情况填就行，不是很重要
group = "com.qiqv"
version = "1.0-SNAPSHOT"

// 插件等依赖的下载地址，默认会去中央仓库下载，这里我们一般是会改为直接去idea官网下载或者是用其他镜像
repositories {
    maven("https://maven.aliyun.com/repository/central/");
    maven("https://maven.aliyun.com/repository/public/");
    maven("https://maven.aliyun.com/repository/google/");
    maven("https://maven.aliyun.com/repository/jcenter/");
    maven("https://maven.aliyun.com/repository/gradle-plugin");
}

dependencies { 
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.3")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.3")
    testRuntimeOnly("org.junit.vintage:junit-vintage-engine:5.9.3")
    testImplementation("org.junit.jupiter:junit-jupiter-params:5.9.3")
}

// 这里是很重要的配置，定义了gradle构建时依赖的idea版本，我们进行插件调试的时候，会使用这里定义的idea版本来进行测试的。
intellij {
    version.set("2022.2.5")
    type.set("IC") // Target IDE Platform

    plugins.set(listOf(/* Plugin Dependencies */))
}

// 定义构建的任务，主要是改一下编译的jdk版本，插件适用的idea版本等信息
tasks {
    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
        options.encoding = "utf-8"
        options.compilerArgs = listOf("-Xlint:unchecked", "-Xlint:deprecation", "-parameters")
    }

    withType<JavaExec> {
        systemProperty("file.encoding", "utf-8")
    }
    
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }

    patchPluginXml {
        sinceBuild.set("222")
        untilBuild.set("232.*")
    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }
}
